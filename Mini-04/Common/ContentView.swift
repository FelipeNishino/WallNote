//
//  ContentView.swift
//  Mini-04
//
//  Created by Rogerio Lucon on 29/09/21.
//

import SwiftUI

struct ContentView: View {
    var body: some View {
        #if Manager
        Text("Hello, Manager!")
            .padding()
        #elseif Client
        Text("Hello, Client!")
            .padding()
        #endif
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
