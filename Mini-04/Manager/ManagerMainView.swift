//
//  ManagerMainView.swift
//  Manager
//
//  Created by Rogerio Lucon on 29/09/21.
//

import SwiftUI

struct ManagerMainView: View {
    var body: some View {
        Text(/*@START_MENU_TOKEN@*/"Hello, World!"/*@END_MENU_TOKEN@*/)
    }
}

struct ManagerMainView_Previews: PreviewProvider {
    static var previews: some View {
        ManagerMainView()
    }
}
