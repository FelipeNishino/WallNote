//
//  ClientMainView.swift
//  Client
//
//  Created by Rogerio Lucon on 29/09/21.
//

import SwiftUI

struct ClientMainView: View {
    var body: some View {
        Text(/*@START_MENU_TOKEN@*/"Hello, World!"/*@END_MENU_TOKEN@*/)
    }
}

struct ClientMainView_Previews: PreviewProvider {
    static var previews: some View {
        ClientMainView()
    }
}
